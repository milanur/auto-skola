INSERT INTO adresa (id, ulica, broj) VALUES (1,'Bulevar Cara Lazara', 5);
INSERT INTO adresa (id, ulica, broj) VALUES (2, 'Dalmatinska', 7);

INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN',1);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK',2);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK',2);


INSERT INTO auto_skola(id,naziv,godina_osnivanja,broj_vozila) VALUES (1,'Astra',1999,3);
INSERT INTO auto_skola(id,naziv,godina_osnivanja,broj_vozila) VALUES (2,'Signal',2000,5);
INSERT INTO auto_skola(id,naziv,godina_osnivanja,broj_vozila) VALUES (3,'Zmigavac',2012,1);

INSERT INTO polaganje(id,broj_mesta,datum,auto_skola_id) VALUES (1,12,'12/06/2022',1);
INSERT INTO polaganje(id,broj_mesta,datum,auto_skola_id) VALUES (2,13,'15/02/2021',2);
INSERT INTO polaganje(id,broj_mesta,datum,auto_skola_id) VALUES (3,14,'24/04/2022',3);
INSERT INTO polaganje(id,broj_mesta,datum,auto_skola_id) VALUES (4,15,'11/02/2023',1);

INSERT INTO polaznik(id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_Voznju,polozio) VALUES (1,'Milan','Simic',1999,'Sid',1, true,true,true);
INSERT INTO polaznik(id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_Voznju,polozio) VALUES (2,'Stevan','Sremac',2000,'Novi Sad ',2,true,true,true);
INSERT INTO polaznik(id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_Voznju,polozio) VALUES (3,'Janko','Markovic',1999,'Beograd',3,false,true,false);