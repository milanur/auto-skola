package testApp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import testApp.dto.PolaganjeDTO;
import testApp.model.Polaganje;
import testApp.services.PolaganjeService;
import testApp.support.PolaganjeToPolaganjeDTO;

@RestController
@RequestMapping(value = "/api/polaganja", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class PolaganjeController {
	
	@Autowired
	public PolaganjeService polaganjeService;
	
	@Autowired
	public PolaganjeToPolaganjeDTO polaganjeToPolaganjeDTO;
	
	@GetMapping
	ResponseEntity<List<PolaganjeDTO>> getAll(){
		
		List<Polaganje> polaganja = new ArrayList<>();
		polaganja = polaganjeService.findAll();
		
		return new ResponseEntity<>(polaganjeToPolaganjeDTO.convert(polaganja),HttpStatus.OK);
		
		
	}

}
