package testApp.controller;


import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.security.access.prepost.PreAuthorize;



import testApp.dto.AutoSkolaDTO;
import testApp.dto.PolaganjeDTO;
import testApp.model.AutoSkola;
import testApp.model.Polaganje;
import testApp.services.AutoSkolaService;
import testApp.services.PolaganjeService;
import testApp.support.AutoToAutoDTO;
import testApp.support.PolaganjeToPolaganjeDTO;

@RestController
@RequestMapping(value = "/api/autoSkole", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class AutoSkolaController {
	
@Autowired
public AutoSkolaService asService;

@Autowired
public AutoToAutoDTO autoTOautoDTO;

@Autowired
public PolaganjeService polaganjeService;

@Autowired
public PolaganjeToPolaganjeDTO polaganjeToPolaganjeDTO;

//@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
@GetMapping
public ResponseEntity<List<AutoSkolaDTO>> getAll(){
	List<AutoSkola> autoSkole = new ArrayList<>();
	autoSkole = asService.findAll();
	return new ResponseEntity<>(autoTOautoDTO.conver(autoSkole), HttpStatus.OK);
	
	
}
@GetMapping("/{id}/polaganja")
public ResponseEntity<List<PolaganjeDTO>> findByPolaganjaId(@PathVariable @Positive(message = "Id must be positive.")  Long id){
    List<Polaganje> polaganje = polaganjeService.findByAutoSkolaId(id);

    return new ResponseEntity<>(polaganjeToPolaganjeDTO.convert(polaganje), HttpStatus.OK);
}

}
