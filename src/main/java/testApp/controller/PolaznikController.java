package testApp.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import testApp.dto.PolaznikDTO;
import testApp.model.Polaznik;
import testApp.services.PolaznikService;
import testApp.support.PolaznikDTOtoPolaznik;
import testApp.support.PolaznikToPolaznikDTO;


@RestController
@RequestMapping(value = "/api/polaznici", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class PolaznikController {
	
	@Autowired
	public PolaznikService polaznikService;
	
	@Autowired
	public PolaznikToPolaznikDTO polaznikToPolaznikDTO;
	
	@Autowired
	public PolaznikDTOtoPolaznik polaznikDtoToPolaznik;
	
	@GetMapping("/{id}")
	public ResponseEntity<PolaznikDTO> getOne(@PathVariable Long id) {
		Polaznik polaznik = polaznikService.findOne(id);
		
		if(polaznik!=null) {
			return new ResponseEntity<>(polaznikToPolaznikDTO.convert(polaznik), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	
	@RequestMapping
	ResponseEntity<List<PolaznikDTO>> getAll(@RequestParam(value = "polaznikIme", required = false) String polaznikIme,
											@RequestParam(value = "autoSkolaId", required =  false) Long autoSkolaId,
											@RequestParam(value = "pageNo", defaultValue = "0")int pageNo){
		
		Page<Polaznik> page = null;
		
		if(polaznikIme != null || autoSkolaId != null) {
			page = polaznikService.search(polaznikIme, autoSkolaId, pageNo);
		} else {
			page = polaznikService.all(pageNo);
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		
	
		return new ResponseEntity<>(polaznikToPolaznikDTO.convert(page.getContent()), headers, HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PolaznikDTO> create(@Valid @RequestBody PolaznikDTO polaznikDTO) {
		Polaznik polaznik = polaznikDtoToPolaznik.convert(polaznikDTO);
		Polaznik sacuvaniPolaznik = polaznikService.save(polaznik);

		return new ResponseEntity<>(polaznikToPolaznikDTO.convert(sacuvaniPolaznik), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	ResponseEntity<Void> delete(@PathVariable Long id) {

		Polaznik obrisani = polaznikService.delete(id);
		if(obrisani != null) {
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<PolaznikDTO> edit(@PathVariable Long id, @Valid @RequestBody PolaznikDTO polazniciDTO) {
		if(!id.equals(polazniciDTO.getId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} 
		Polaznik polaznik = polaznikDtoToPolaznik.convert(polazniciDTO);
		Polaznik sacuvanPolaznik = polaznikService.upadate(polaznik);
		
		return new ResponseEntity<>(polaznikToPolaznikDTO.convert(sacuvanPolaznik),HttpStatus.OK);
	}
}
