package testApp.services;

import java.util.List;

import org.springframework.stereotype.Service;

import testApp.model.Polaganje;

@Service
public interface PolaganjeService {
	
	List<Polaganje> findByAutoSkolaId(Long id);
	
	List<Polaganje> findAll();

}
