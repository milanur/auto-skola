package testApp.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import testApp.model.Polaganje;
import testApp.repository.PolaganjeRepository;
import testApp.services.PolaganjeService;

@Service
public class JpaPolaganjeService implements PolaganjeService {

	@Autowired
	public PolaganjeRepository polaganjeRepository;
	
	@Override
	public List<Polaganje> findByAutoSkolaId(Long autoSkolaID) {	
		return polaganjeRepository.findByAutoSkolaId(autoSkolaID);
	}

	@Override
	public List<Polaganje> findAll() {
		
		return polaganjeRepository.findAll();
	}

}
