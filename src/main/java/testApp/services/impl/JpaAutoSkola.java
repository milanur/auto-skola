package testApp.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import testApp.model.AutoSkola;
import testApp.repository.AutoSkolaRepository;
import testApp.services.AutoSkolaService;

@Service
public class JpaAutoSkola implements AutoSkolaService{
	
	@Autowired
	public AutoSkolaRepository autoSkolaRepository;

	@Override
	public List<AutoSkola> findAll() {
		
		return autoSkolaRepository.findAll();
	}

	@Override
	public AutoSkola getOne(Long id) {
		
		return autoSkolaRepository.getOne(id);
	}

}
