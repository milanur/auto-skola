package testApp.services.impl;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import testApp.model.Polaznik;
import testApp.repository.PolaznikRepository;
import testApp.services.PolaznikService;

@Service
public class JpaPolaznik implements PolaznikService{
	
	@Autowired
	public PolaznikRepository polaznikRepository;

	

	@Override
	public Polaznik findOne(Long id) {
		
		return polaznikRepository.getOne(id);
	}

	@Override
	public Page<Polaznik> all(int page) {
		
		return polaznikRepository.findAll(PageRequest.of(page, 2));
	}

@Override
	public Page<Polaznik> search(String polaznikIme, Long autoSkolaId, int page) {
		if(polaznikIme!=null) {
			polaznikIme = "%" + polaznikIme + "%";
		}
		return polaznikRepository.search(polaznikIme, autoSkolaId, PageRequest.of(page, 2));
		
	}


@Override
	
	public List<Polaznik> find(String ime, Long autoSkolaId) {
	if(ime != null) {
		ime = "";
	}
	if(autoSkolaId != null) {
		return polaznikRepository.findByImeIgnoreCaseContainsAndAutoSkolaId(ime, autoSkolaId);
	}
	
	return polaznikRepository.findByImeIgnoreCaseContains(ime);
}


@Override
	
	public Polaznik upadate(Polaznik polaznik) {
	
	return polaznikRepository.save(polaznik);
}


	@Override	
	public Polaznik delete(Long id) {
	Optional<Polaznik> polaznik = polaznikRepository.findById(id);
	if(polaznik.isPresent()) {
		polaznikRepository.deleteById(id);
		return polaznik.get();
	}
	return null;
}

	@Override
	public Polaznik save(Polaznik polaznik) {
		
		return polaznikRepository.save(polaznik);
	}

}
