package testApp.services;



import java.util.List;
import java.util.Optional;

import testApp.model.Adresa;

public interface AdresaService {

    Optional<Adresa> findOne(Long id);

    List<Adresa> findAll();

    Adresa save(Adresa adresa);

    void delete(Long id);

}
