package testApp.services;

import java.util.List;

import org.springframework.stereotype.Service;

import testApp.model.AutoSkola;

@Service
public interface AutoSkolaService {
	
	List<AutoSkola> findAll();
	
	AutoSkola getOne(Long id);

}
