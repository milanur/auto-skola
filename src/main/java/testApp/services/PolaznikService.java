package testApp.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import testApp.model.Polaznik;

@Service
public interface PolaznikService {
	
	Page<Polaznik> all(int page);
	
	Polaznik findOne(Long id);
	
	Page<Polaznik> search(String polaznikIme, Long autoSkolaId, int pageNum);
	
	List<Polaznik> find (String ime, Long autoSkolaId);
	
	Polaznik upadate(Polaznik polaznik);
	
	Polaznik delete(Long id);
	
	Polaznik save(Polaznik polaznik);
	
	

}
