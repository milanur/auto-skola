package testApp.dto;

import org.hibernate.validator.constraints.Length;

public class AutoSkolaDTO {
	
	private Long id;
	@Length(max = 50)
	private String naziv;
	
	private int broj_vozila;

	private int godina_osnivanja;
	
	public int getGodina_osnivanja() {
		return godina_osnivanja;
	}

	public void setGodina_osnivanja(int godina_osnivanja) {
		this.godina_osnivanja = godina_osnivanja;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBroj_vozila() {
		return broj_vozila;
	}

	public void setBroj_vozila(int broj_vozila) {
		this.broj_vozila = broj_vozila;
	}
	
	

}
