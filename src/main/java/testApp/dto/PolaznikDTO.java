package testApp.dto;

import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;


@Component
public class PolaznikDTO {
	
	private Long id;
	@Length(max = 30)
	private String ime;
	
	private String prezime;
	
	private String mesto;
	
	private int godina_rodjenja;
	
	private Boolean odslusaoTeoriju;
	
	private Boolean odradioVoznju;
	
	private Boolean polozio;
		
	private String autoSkolazNaziv;
	
	private Long autoSkolaId;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public int getGodina_rodjenja() {
		return godina_rodjenja;
	}
	
	

	public String getMesto() {
		return mesto;
	}

	public void setMesto(String mesto) {
		this.mesto = mesto;
	}

	public void setGodina_rodjenja(int godina_rodjenja) {
		this.godina_rodjenja = godina_rodjenja;
	}

	public Boolean getOdslusaoTeoriju() {
		return odslusaoTeoriju;
	}

	public void setOdslusaoTeoriju(Boolean odslusaoTeoriju) {
		this.odslusaoTeoriju = odslusaoTeoriju;
	}

	public Boolean getOdradioVoznju() {
		return odradioVoznju;
	}

	public void setOdradioVoznju(Boolean odradioVoznju) {
		this.odradioVoznju = odradioVoznju;
	}

	public Boolean getPolozio() {
		return polozio;
	}

	public void setPolozio(Boolean polozio) {
		this.polozio = polozio;
	}

	public String getAutoSkolazNaziv() {
		return autoSkolazNaziv;
	}

	public void setAutoSkolazNaziv(String autoSkolazNaziv) {
		this.autoSkolazNaziv = autoSkolazNaziv;
	}

	public Long getAutoSkolaId() {
		return autoSkolaId;
	}

	public void setAutoSkolaId(Long autoSkolaId) {
		this.autoSkolaId = autoSkolaId;
	}


	

}
	
	
	
	


