package testApp.dto;

import javax.validation.constraints.Positive;

import org.springframework.stereotype.Component;

@Component
public class PolaganjeDTO {

	private Long id;
	
	private String datum;
	@Positive 
	private int broj_mesta;

	private AutoSkolaDTO autoSkola;
	
	public Long getId() {
		return id;
	}
	
	

	public AutoSkolaDTO getAutoSkola() {
		return autoSkola;
	}



	public void setAutoSkola(AutoSkolaDTO autoSkola) {
		this.autoSkola = autoSkola;
	}



	public void setId(Long id) {
		this.id = id;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public int getBroj_mesta() {
		return broj_mesta;
	}

	public void setBroj_mesta(int broj_mesta) {
		this.broj_mesta = broj_mesta;
	}
	
	
}
