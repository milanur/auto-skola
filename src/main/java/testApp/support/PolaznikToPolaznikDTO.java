package testApp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


import testApp.dto.PolaznikDTO;
import testApp.model.Polaznik;

@Component
public class PolaznikToPolaznikDTO implements Converter<Polaznik, PolaznikDTO> {
	
	@Override
	public PolaznikDTO convert(Polaznik polaznik) {
		PolaznikDTO dto = new PolaznikDTO();
		dto.setId(polaznik.getId());
		dto.setIme(polaznik.getIme());
		dto.setPrezime(polaznik.getPrezime());
		dto.setGodina_rodjenja(polaznik.getGodina_rodjenja());
		dto.setMesto(polaznik.getMesto());
		dto.setOdradioVoznju(polaznik.getOdradioVoznju());
		dto.setOdslusaoTeoriju(polaznik.getodslusao_teoriju());
		dto.setPolozio(polaznik.getPolozio());
		dto.setAutoSkolazNaziv(polaznik.getAutoSkola().getNaziv());

		return dto;
	}


	public List<PolaznikDTO> convert(List<Polaznik> polaznici){
		List<PolaznikDTO> polaznikDTO = new ArrayList<>();
				
				for(Polaznik s : polaznici) {
					polaznikDTO.add(convert(s));
				}
		return polaznikDTO;
	
	
	
	}

}
