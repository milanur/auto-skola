package testApp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import testApp.dto.PolaznikDTO;
import testApp.model.AutoSkola;
import testApp.model.Polaznik;
import testApp.services.AutoSkolaService;
import testApp.services.PolaznikService;

@Component
public class PolaznikDTOtoPolaznik {

	@Autowired
	private PolaznikService polaznikService;
	
	@Autowired
	private AutoSkolaService autoSkolaService;

	
	public Polaznik convert(PolaznikDTO dto) {
		Polaznik entity;
		
		if(dto.getId() == null) {
			entity = new Polaznik();
		}else {
			entity = polaznikService.findOne(dto.getId());
		}
		
		if(entity!=null) {
			entity.setIme(dto.getIme());
			entity.setPrezime(dto.getPrezime());
			entity.setGodina_rodjenja(dto.getGodina_rodjenja());
			entity.setMesto(dto.getMesto());
			entity.setodslusao_teoriju(dto.getOdslusaoTeoriju());
			entity.setOdradioVoznju(dto.getOdradioVoznju());
			entity.setPolozio(dto.getPolozio());
			
			AutoSkola autoSkola = autoSkolaService.getOne(dto.getAutoSkolaId());
			entity.setAutoSkola(autoSkola);
		}
		
		
		
		return entity;
	}
}
