package testApp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;



import testApp.dto.PolaganjeDTO;
import testApp.model.Polaganje;

@Component
public class PolaganjeToPolaganjeDTO implements Converter<Polaganje, PolaganjeDTO> {

	@Autowired
	private AutoToAutoDTO autoSkolaDTO;
	
	@Override
	public PolaganjeDTO convert(Polaganje polaganje) {
		PolaganjeDTO dto = new PolaganjeDTO();
		dto.setId(polaganje.getId());
		dto.setBroj_mesta(polaganje.getBroj_mesta());
		dto.setDatum(polaganje.getDatum());
		dto.setAutoSkola(autoSkolaDTO.convert(polaganje.getAutoSkola()));
		
		return dto;
	}
	
	 public List<PolaganjeDTO> convert(List<Polaganje> polaganje){
	        List<PolaganjeDTO> polaganjeDTO = new ArrayList<>();

	        for(Polaganje polaganja : polaganje) {
	        	polaganjeDTO.add(convert(polaganja));
	        }

	        return polaganjeDTO;
	    }
	

}
