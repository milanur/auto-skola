package testApp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import testApp.dto.AutoSkolaDTO;
import testApp.model.AutoSkola;

@Component
public class AutoToAutoDTO implements Converter<AutoSkola, AutoSkolaDTO> {
	
	@Override
	public AutoSkolaDTO convert(AutoSkola autoSkola) {
		AutoSkolaDTO dto = new AutoSkolaDTO();
		dto.setId(autoSkola.getId());
		dto.setBroj_vozila(autoSkola.getBroj_vozila());
		dto.setNaziv(autoSkola.getNaziv());
		dto.setGodina_osnivanja(autoSkola.getgodina_osnivanja());
		return dto;
	}
	
	public List<AutoSkolaDTO> conver(List<AutoSkola> autoSkole){
		List<AutoSkolaDTO> autoDTO = new ArrayList<>();
				
				for(AutoSkola s : autoSkole) {
					autoDTO.add(convert(s));
				}
		return autoDTO;
	}

}
