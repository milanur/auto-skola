package testApp.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class AutoSkola {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	
	private Long id;
	@Column(nullable = false, unique = true)	
	private String naziv;
	
	@Column(nullable = false)
	private int godina_osnivanja;
	
	@Column	
	private int broj_vozila;
	
	@OneToMany(mappedBy = "autoSkola", fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	private List<Polaznik> polaznici = new ArrayList<>();
	
	@OneToMany(mappedBy = "autoSkola", fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	private List<Polaganje> polaganja = new ArrayList<>();

	public AutoSkola() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AutoSkola(Long id, String naziv, int godina_osnivanja, int broj_vozila, ArrayList<Polaznik> polaznici,
			ArrayList<Polaganje> polaganja) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.godina_osnivanja = godina_osnivanja;
		this.broj_vozila = broj_vozila;
		this.polaznici = polaznici;
		this.polaganja = polaganja;
	}

	@Override
	public String toString() {
		return "Auto_skola [id=" + id + ", naziv=" + naziv + ", godina_osnivanja=" + godina_osnivanja + ", broj_vozila="
				+ broj_vozila + ", polaznici=" + polaznici + ", polaganja=" + polaganja + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutoSkola other = (AutoSkola) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getgodina_osnivanja() {
		return godina_osnivanja;
	}

	public void setgodina_osnivanja(int godina_osnivanja) {
		this.godina_osnivanja = godina_osnivanja;
	}

	public int getBroj_vozila() {
		return broj_vozila;
	}

	public void setBroj_vozila(int broj_vozila) {
		this.broj_vozila = broj_vozila;
	}

	public List<Polaznik> getPolaznici() {
		return polaznici;
	}

	public void setPolaznici(ArrayList<Polaznik> polaznici) {
		this.polaznici = polaznici;
	}

	public List<Polaganje> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(ArrayList<Polaganje> polaganja) {
		this.polaganja = polaganja;
	}
	
	
	
	

}
