package testApp.model;


import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Polaganje {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true)
	private int broj_mesta;
	
	@Column(unique = true)
	private String datum;
	
	@ManyToOne
	private AutoSkola autoSkola;

	public Polaganje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Polaganje(Long id, int broj_mesta, String datum, AutoSkola autoSkola) {
		super();
		this.id = id;
		this.broj_mesta = broj_mesta;
		this.datum = datum;
		this.autoSkola = autoSkola;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Polaganje other = (Polaganje) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBroj_mesta() {
		return broj_mesta;
	}

	public void setBroj_mesta(int broj_mesta) {
		this.broj_mesta = broj_mesta;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public AutoSkola getAutoSkola() {
		return autoSkola;
	}

	public void setAutoSkola(AutoSkola autoSkola) {
		this.autoSkola = autoSkola;
	}

	@Override
	public String toString() {
		return "Polaganje [id=" + id + ", broj_mesta=" + broj_mesta + ", datum=" + datum + ", autoSkola=" + autoSkola
				+ "]";
	}
	
	
}
