package testApp.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Polaznik {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String ime;
	@Column(nullable = false)
	private String prezime;
	@Column
	private int godina_rodjenja;
	@Column
	private String mesto;
	@Column
	private Boolean odslusao_teoriju = false;
	@Column
	private Boolean odradioVoznju = false;
	@Column
	private Boolean polozio = false;
	
	@ManyToOne
	private AutoSkola autoSkola;

	public Polaznik() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Polaznik(Long id, String ime, String prezime, int godina_rodjenja, String mesto, Boolean odslusao_teoriju,
			Boolean odradioVoznju, Boolean polozio, AutoSkola autoSkola) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.godina_rodjenja = godina_rodjenja;
		this.mesto = mesto;
		this.odslusao_teoriju = odslusao_teoriju;
		this.odradioVoznju = odradioVoznju;
		this.polozio = polozio;
		this.autoSkola = autoSkola;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Polaznik other = (Polaznik) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public int getGodina_rodjenja() {
		return godina_rodjenja;
	}

	public void setGodina_rodjenja(int godina_rodjenja) {
		this.godina_rodjenja = godina_rodjenja;
	}

	public String getMesto() {
		return mesto;
	}

	public void setMesto(String mesto) {
		this.mesto = mesto;
	}

	public Boolean getodslusao_teoriju() {
		return odslusao_teoriju;
	}

	public void setodslusao_teoriju(Boolean odslusao_teoriju) {
		this.odslusao_teoriju = odslusao_teoriju;
	}

	public Boolean getOdradioVoznju() {
		return odradioVoznju;
	}

	public void setOdradioVoznju(Boolean odradioVoznju) {
		this.odradioVoznju = odradioVoznju;
	}

	public Boolean getPolozio() {
		return polozio;
	}

	public void setPolozio(Boolean polozio) {
		this.polozio = polozio;
	}

	public AutoSkola getAutoSkola() {
		return autoSkola;
	}

	public void setAutoSkola(AutoSkola autoSkola) {
		this.autoSkola = autoSkola;
	}

	@Override
	public String toString() {
		return "Polaznik [id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", godina_rodjenja=" + godina_rodjenja
				+ ", mesto=" + mesto + ", odslusao_teoriju=" + odslusao_teoriju + ", odradioVoznju=" + odradioVoznju
				+ ", polozio=" + polozio + ", autoSkola=" + autoSkola + "]";
	}
	
	
	
}
