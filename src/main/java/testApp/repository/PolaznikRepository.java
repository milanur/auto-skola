package testApp.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import testApp.model.Polaznik;

@Repository
public interface PolaznikRepository extends JpaRepository<Polaznik, Long> {
	
	@Query("SELECT v FROM Polaznik v WHERE" +
			"(:polaznikIme = NULL OR v.ime LIKE :polaznikIme) AND " + 
			"(:autoSkolaId = NULL OR v.autoSkola.id = :autoSkolaId)")
	Page<Polaznik> search(@Param("polaznikIme") String polaznikIme, @Param("autoSkolaId") Long autoSkolaId, Pageable pageable);
	List<Polaznik> findByImeIgnoreCaseContainsAndAutoSkolaId(String ime, Long autoSkolaId);
	List<Polaznik> findByImeIgnoreCaseContains(String ime);

}
