package testApp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import testApp.model.Adresa;




@Repository
public interface AdresaRepository extends JpaRepository<Adresa, Long> {

}
