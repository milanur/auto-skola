package testApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import testApp.model.AutoSkola;

@Repository
public interface AutoSkolaRepository extends JpaRepository<AutoSkola, Long> {

}
