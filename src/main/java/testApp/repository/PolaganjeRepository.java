package testApp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import testApp.model.Polaganje;

@Repository
public interface PolaganjeRepository extends JpaRepository<Polaganje, Long> {
	
	List<Polaganje> findByAutoSkolaId(Long autoSkolaId);

}
